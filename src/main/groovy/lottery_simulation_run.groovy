@Grapes([
        @Grab(group = 'ca.umontreal.iro.simul', module = 'ssj', version = '3.2.0')
])

import groovy.transform.BaseScript
import umontreal.ssj.rng.RandomPermutation
import umontreal.ssj.rng.WELL512

@BaseScript Environment env

final warmUpPeriod = SIMULATION_SETTINGS.WARMUP_PERIOD
final simulationRuns = SIMULATION_SETTINGS.SIMULATION_RUNS
final simulationRunSamples = SIMULATION_SETTINGS.SIMULATION_RUN_SAMPLES
final RND_STREAM = new WELL512()
final DRAW_UNIVERSE = (1..33).toArray(new Integer[33]) //our universe of possible numbers

//lets warm up the WELL PRNG used just to be safe and prevent any bias, although is not required
warmUpPeriod.each {
    RandomPermutation.shuffle(DRAW_UNIVERSE, RND_STREAM)
}

simulationRuns.each { simulationRun ->
    withSimulationFilePrinter(simulationRun) { printer ->
        simulationRunSamples.each {
            RandomPermutation.shuffle(DRAW_UNIVERSE, RND_STREAM)
            printer.printRecord(DRAW_UNIVERSE[0],
                    DRAW_UNIVERSE[1],
                    DRAW_UNIVERSE[2],
                    DRAW_UNIVERSE[3],
                    DRAW_UNIVERSE[4],
                    DRAW_UNIVERSE[5])
        }
    }
}
