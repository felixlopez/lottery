@Grapes([
        @Grab(group='org.apache.commons', module='commons-math3', version='3.6.1')
])

import groovy.transform.BaseScript
import org.apache.commons.math3.stat.inference.ChiSquareTest

@BaseScript Environment env

def stats = new ChiSquareTest();
def m = 6.0
def M = 33.0
def adjustmentFactor = 1.185185185 //(M-1)/(M-m)=(33-1)/(33-6)=1.185185185

[20,25,50,100].each { groupSize ->
    withProcessedFilePrinter("loto_chi_square_group_${groupSize}") { printer ->
        readProcessedFile("loto_frequencies_grouped_${groupSize}") { record ->
            def chiSquare = stats.chiSquare((0..32).collect { groupSize * m / M } as double[],
                    (1..33).collect { (Long.parseLong(record.get(it))) } as long[])
            printer.printRecord(chiSquare * adjustmentFactor)
        }
    }
}
