import groovy.transform.BaseScript

@BaseScript Environment env


def simulationRuns = SIMULATION_SETTINGS.SIMULATION_RUNS;
def simulationRunSamples = SIMULATION_SETTINGS.SIMULATION_RUN_SAMPLES
def maximumGapFoundInSimulation = 0;
def gapsFrequencies = ([:] as TreeMap).withDefault { 0 }
def frequencies = ([:] as TreeMap).withDefault { 0 }
def countGaps = 0;


withSimulation { simulation ->
    withProcessedFilePrinter('loto_simulation_gaps_maximum') { printer ->
        simulationRuns.each {
            def gap = [:].withDefault { 0 }
            def maximumGapFoundInHistorySample = 0;

            simulationRunSamples.each {
                def drawnNumbers = simulation.nextDraw()

                ((1..33) - drawnNumbers).each {
                    gap[it]++
                }

                drawnNumbers.each { obs ->
                    if (gap[obs] > maximumGapFoundInHistorySample) {
                        maximumGapFoundInHistorySample = gap[obs]
                    }
                    frequencies[obs]++
                    countGaps++
                    gapsFrequencies[gap[obs]]++
                    gap[obs] = 0
                }

            }
            printer.printRecord(maximumGapFoundInHistorySample)
            if (maximumGapFoundInSimulation < maximumGapFoundInHistorySample)
                maximumGapFoundInSimulation = maximumGapFoundInHistorySample
        }

        printer.printRecord(maximumGapFoundInSimulation)
    }
}

withProcessedFilePrinter('loto_simulation_gaps_frequencies') { printer ->
    gapsFrequencies.each { observedGap ->
        printer.printRecord(observedGap.key, observedGap.value)
    }

}

withProcessedFilePrinter('loto_simulation_drawn_frequencies') { printer ->
    frequencies.each { observedFrequencies ->
        printer.printRecord(observedFrequencies.key, observedFrequencies.value)
    }
}


println(countGaps)