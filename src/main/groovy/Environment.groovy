@Grapes([
        @Grab(group = 'org.apache.commons', module = 'commons-csv', version = '1.4'),
        @Grab(group = 'org.apache.commons', module = 'commons-io', version = '1.3.2'),
        @Grab(group = 'ca.umontreal.iro.simul', module = 'ssj', version = '3.2.0')
])

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.csv.CSVRecord
import org.apache.commons.io.FilenameUtils

import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

abstract class Environment extends Script {
    private final DEFAULT_FILE_FORMAT = CSVFormat.DEFAULT;
    private final BASE_DIR = '../../../data'
    private final RAW_DATA_DIR = "${BASE_DIR}/data/raw"
    private final PROCESSED_DATA_DIR = "${BASE_DIR}/data/processed"
    private final SIMULATION_DATA_DIR = "${BASE_DIR}/simulation"
    protected final SIMULATION_SETTINGS = [WARMUP_PERIOD         : (1..Integer.MAX_VALUE),
                                           SIMULATION_RUNS       : (1..10000),
                                           SIMULATION_RUN_SAMPLES: (1..1504)]


    private getSimulationDataFile() {
        new File(SIMULATION_DATA_DIR, 'simulation_runs.csv.gz')
    }

    public File getDataFile(int year) {
        new File(RAW_DATA_DIR, "loto_${year}.csv")
    }

    public File getProcessedFile(String fileName) {
        new File(PROCESSED_DATA_DIR, FilenameUtils.getBaseName(fileName) + '.csv')
    }

    private getCSVPrinter(File csvFile, Closure body) {
        def writer = new FileWriter(csvFile)
        def csvPrinter = new CSVPrinter(writer, DEFAULT_FILE_FORMAT)
        body(csvPrinter)
        csvPrinter.close()
    }

    private getGZCSVPrinter(File csvFile, Closure body) {
        GZIPOutputStream gzOutputStream = new GZIPOutputStream(new FileOutputStream(csvFile))

        gzOutputStream.withWriter { writer ->
            def csvPrinter = new CSVPrinter(writer, DEFAULT_FILE_FORMAT)
            body(csvPrinter)
        }

        gzOutputStream.finish()
        gzOutputStream.close()
    }

    public void withSimulationFilePrinter(int simulationRun, Closure body) {
        def csvFile = getSimulationDataFile(simulationRun)
        getGZCSVPrinter(csvFile, body)
    }

    public void withProcessedFilePrinter(String fileName, Closure body) {
        def csvFile = getProcessedFile(fileName)
        getCSVPrinter(csvFile, body)
    }

    public void withSimulation(Closure body) {
        FileInputStream fis = new FileInputStream(getSimulationDataFile());
        GZIPInputStream gis = new GZIPInputStream(fis);
        InputStreamReader isr = new InputStreamReader(gis);
        BufferedReader br = new BufferedReader(isr);
        CSVParser parser = new CSVParser(br,DEFAULT_FILE_FORMAT )

        def simulation = new Expando(parser: parser, drawsSource: parser.iterator())
        simulation.nextDraw = {
            CSVRecord record = drawsSource.next();
            [Integer.parseInt(record.get(0)),
             Integer.parseInt(record.get(1)),
             Integer.parseInt(record.get(2)),
             Integer.parseInt(record.get(3)),
             Integer.parseInt(record.get(4)),
             Integer.parseInt(record.get(5))]
        }

        body(simulation)

        parser.close()
    }

    public void readProcessedFile(String fileName, Closure body) {
        def inFile = getProcessedFile(fileName)
        Reader reader = new FileReader(inFile);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader)
        for (CSVRecord record : records) {
            body(record)
        }
        reader.close()
    }

    public void readDataFile(int year, Closure body) {
        def file = getDataFile(year)
        def inReader = new FileReader(file);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(inReader)
        for (CSVRecord record : records) {
            def winners = record.get(1).split('-')
            int obs01 = Integer.parseInt(winners[0]),
                obs02 = Integer.parseInt(winners[1]),
                obs03 = Integer.parseInt(winners[2]),
                obs04 = Integer.parseInt(winners[3]),
                obs05 = Integer.parseInt(winners[4]),
                obs06 = Integer.parseInt(winners[5])

            body(obs01, obs02, obs03, obs04, obs05, obs06)
        }
        inReader.close()
    }

}
