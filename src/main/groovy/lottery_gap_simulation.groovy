import groovy.transform.BaseScript

@BaseScript Environment env

final simulationsDraws = (1..10000)
final drawsHistorySample = (1..1504)

def maximumGapFoundInSimulation = 0;

withProcessedFilePrinter('loto_max_observed_gaps_in_simulation') { printer ->
    simulationsDraws.each {
        def gapsFrequencies = [:].withDefault { 0 }
        def gap = [:].withDefault { 0 }
        def maximumGapFoundInHistorySample = 0;

        drawsHistorySample.each {
            def drawnNumbers = nextLotoDraw()

            ((1..33) - drawnNumbers).each {
                gap[it]++
            }

            drawnNumbers.each { obs ->
                if (gap[obs] > maximumGapFoundInHistorySample) {
                    maximumGapFoundInHistorySample = gap[obs]
                }
                gapsFrequencies[gap[obs]]++
                gap[obs] = 0
            }

        }
        printer.printRecord(maximumGapFoundInHistorySample)
        if (maximumGapFoundInSimulation < maximumGapFoundInHistorySample)
            maximumGapFoundInSimulation = maximumGapFoundInHistorySample
    }

    printer.printRecord(maximumGapFoundInSimulation)
}